'use strict';

const gulp = require('gulp');
const utils = require('./utils');

// gulp patch       : used to handle operating system specific commands
gulp.task('patch', patch);

/*****************/

function patch(done) {
    const isWindows = (process.platform === 'win32');

    const command = {
        windows: 'xcopy .\\patches\\covalent-core-data-table.js .\\node_modules\\@covalent\\core\\esm5\\ /y',
        linux: 'cp -R ./patches/covalent-core-data-table.js ./node_modules/@covalent/core/esm5/',
    };

    return gulp.series(
        utils.callTask(utils.runInShell(command.windows), isWindows),
        utils.callTask(utils.runInShell(command.linux), !isWindows),
    )(done);
}

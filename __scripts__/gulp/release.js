'use strict';

const fs = require('fs');
const gulp = require('gulp');
const replace = require('gulp-replace');
const args = require('yargs').argv;
const utils = require('./utils');

// gulp zip                     : zip project contents for app-planner
// gulp zip --basic             : zip project contents for app-basic
gulp.task('zip', zipFiles);

// gulp bump --type major       : bumps 1.0.0
// gulp bump --type minor       : bumps 0.1.0
// gulp bump --type patch       : bumps 0.0.2
gulp.task('bump', bumpVersion);

// gulp build-basic             : build app-basic into distribution
gulp.task('build-basic', build('BASIC'));

// gulp build-planner           : build app-planner into distribution
gulp.task('build-planner', build('PLANNER'));

/*****************/

function zipFiles(done) {
    const files = [
        '__scripts__/gulp/**/*',
        '!__scripts__/gulp/publish.js',
        '__scripts__/package.json',
        'patches/**/*',
        'src/**/*',
        '.gitignore',
        'angular.json',
        'gulpfile.js',
        'LICENSE.md',
        'package.json',
        'README.md',
        'tsconfig.json',
        'tslint.json',
    ];

    if (args.basic) {
        // exclude planner files
        files.push(...[
            '!src/app/app-planner/**',
            '!src/app/planner/**',
        ]);
    } else {
        // exclude app basic files
        files.push(...[
            '!src/app/app-basic/**'
        ]);
    }

    return gulp.series(
        utils.zip(files, `frontend${(args.basic) ? '-basic' : ''}`),
    )(done);
}

function bumpVersion(done) {
    const options = { type: args.type || 'patch' };

    const originalVersion = require('../package.json').version;

    utils.validateArgs(options, 'type', ['major', 'minor', 'patch']);

    return gulp.series(
        utils.bumpVersion('./package.json', options.type), // bump version of main package json (for web)
        utils.bumpVersion('./__scripts__/package.json', options.type, './__scripts__'), // bump version of package json (for npm publish)
        updateSidebarVersion(), // update version displayed on sidebar
    )(done);

    function updateSidebarVersion() {
        return (done) => {
            const folder = './src/app/vrp-common/components/main';
            const currentVersion = JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;

            utils.printInfo('Updating to version', currentVersion);

            gulp.src([`${folder}/main.component.ts`])
                .pipe(replace(originalVersion, currentVersion))
                .pipe(gulp.dest(folder))
                .on('end', done);
        };
    }
}

function build(appType) {
    return (done) => {
        const options = { basic: (appType === 'BASIC') };

        const distBuildFolder = utils.getDistFolder(options);
        const npmBuildCommand = options.basic ? 'build-basic' : 'build';

        return gulp.series(
            utils.deleteFolder(distBuildFolder),
            utils.runInShell(`npm run ${npmBuildCommand}`),
            utils.copyFile('LICENSE', 'LICENSE.md', distBuildFolder), // copy license information
            utils.copyFile('package', './__scripts__/package.json', distBuildFolder),
        )(done);
    };
}

'use strict';

const gulp = require('gulp');
const zip = require('gulp-zip');
const git = require('gulp-git');
const bump = require('gulp-bump');
const debug = require('gulp-debug');
const color = require('gulp-color');
const shell = require('gulp-shell');
const rename = require('gulp-rename');
const del = require('del');

const LOG_LEVEL_COLOR = {
    INFO: 'GREEN',
    ERROR: 'YELLOW',
};

let logCount = 0;

function print(message) {
    console.log(`${color(++logCount, 'MAGENTA')} <Gulp> ${message}`);
}

exports.printError = (message) => {
    print(color(message, LOG_LEVEL_COLOR.ERROR));
};

exports.printInfo = (message, emphasis) => {
    print(`${message} ${color(emphasis, LOG_LEVEL_COLOR.INFO)}`);
};

exports.validateArgs = (options, field, accepted) => {
    if (!accepted.includes(options[field])) {
        const err = `Invalid --${field} argument. Supported: ${accepted.join(', ')}`;
        this.printError(err);
        throw 'Error Occurred';
    }
};

exports.bumpVersion = (file, versionType, folder) => {
    return (done) => {
        this.printInfo(`Bump version in `, file);

        gulp.src([file])
            .pipe(bump({ type: versionType }))
            .pipe(gulp.dest(folder || '.'))
            .on('end', done);
    };
};

exports.runInShell = (command) => {
    this.printInfo(`Running command:`, command);

    return shell.task(command);
};

exports.deleteFolder = (folder) => {
    return () => {
        this.printInfo(`Deleting folder`, folder);

        return del([`${folder}/**/*.*`, folder]);
    };
};

exports.copyFolder = (sourceFolder, destinationFolder) => {
    return (done) => {
        this.printInfo(`Copying folder from`, `${sourceFolder} to ${destinationFolder}`);

        gulp.src([`${sourceFolder}/**/*`])
            .pipe(gulp.dest(destinationFolder, { overwrite: true }))
            .on('end', done);
    };
};

exports.deleteFile = (file) => {
    return () => {
        this.printInfo(`Deleting file`, file);

        return del([file]);
    };
}

exports.copyFile = (basename, file, destination) => {
    return (done) => {
        this.printInfo(`Copying file from`, `${file} to ${destination}`);

        gulp.src(file)
            .pipe(rename({ basename: basename }))
            .pipe(gulp.dest(destination, { overwrite: true }))
            .on('end', done);
    };
};

exports.callTask = (taskName, ifCondition) => {
    return (done) => {
        if (ifCondition) {
            gulp.series(taskName)(done);
        } else {
            done();
        }
    };
};

exports.zip = (files, zipName) => {
    return (done) => {
        git.revParse({ args: '--short HEAD' }, (err, hash) => {
            if (err) {
                throw err;
            }

            const filename = `${zipName}-${hash}.zip`;

            this.printInfo('Zipping contents to', filename);

            gulp
                .src(files, { base: '.' })
                .pipe(debug())
                .pipe(zip(filename))
                .pipe(gulp.dest('./'))
                .on('end', done);
        });
    };
};

exports.getDistFolder = (args) => {
    // based on angular.json
    return args.basic ? 'dist-basic' : 'dist-planner';
};

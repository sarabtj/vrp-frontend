'use strict';

const gulp = require('gulp');
const replace = require('gulp-replace');
const args = require('yargs').argv;
const utils = require('./utils');

// gulp publish-simtech             : publish current version in dist to npm
// gulp publish-simtech --rebuild   : rebuild dist before publishing to npm
gulp.task('publish-simtech', publishSIMTech);

// gulp publish-gls                 : generate distribution for gls and publish to npm
// gulp publish-gls --rebuild       : rebuild dist before generate distribution for gls and publish to npm
gulp.task('publish-gls', publishGLS);

// gulp create-y3                   : generate distribution for y3 based on dist folder
// gulp create-y3 --rebuild         : rebuild dist before generate distribution for y3
gulp.task('create-y3', createY3Dist);

/*****************/

function publishSIMTech(done) {
    const config = {
        distFolder: 'dist-npm',
        isAppBasic: false,
        logoFile: '__scripts__/assets/logo_mct.png',
        packageName: '@simtech/vrp-frontend-dist',
        labels: {}, // use default values
    };

    return gulp.series(
        createDist(config),
        npmPublish(config.distFolder),
        zipFolder(config.distFolder),
    )(done);
}

function publishGLS(done) {
    const config = {
        distFolder: 'dist-gls',
        isAppBasic: false,
        logoFile: '__scripts__/assets/logo_gls.png',
        packageName: '@simtech/vrp-frontend-gls',
        labels: {
            title: 'Grocery Logistics of Singapore Pte Ltd',
            header: 'GLS SIMTech LM2',
            company: 'Grocery Logistics Of Singapore Pte Ltd',
        },
    };

    return gulp.series(
        createDist(config),
        npmPublish(config.distFolder),
        zipFolder(config.distFolder),
    )(done);
}

function createY3Dist(done) {
    const config = {
        distFolder: 'dist-y3',
        isAppBasic: true,
        logoFile: '__scripts__/assets/logo_y3.png',
        packageName: '@simtech/vrp-frontend-y3',
        labels: {
            title: 'Vehicle Route Optimization',
            header: 'Y3 VRO',
            company: 'Y3 Technology Pte Ltd',
        },
    };

    return gulp.series(
        createDist(config),
        zipFolder(config.distFolder),
    )(done);
}

function createDist(config) {
    const DEFAULT_HEADERS = { // current values in source code
        title: 'LM2 Logistics Planning', // header in login page
        header: 'SIMTech LM2', // title of web browser
        company: 'Singapore Institute of Manufacturing Technology', // company name on sidebar
    };

    return (done) => {
        const options = { rebuild: args.rebuild, basic: config.isAppBasic };

        const customDistFolder = config.distFolder;
        const logoFile = config.logoFile;
        const labels = {
            title: config.labels.title,
            header: config.labels.header,
            company: config.labels.company,
        };

        utils.printInfo('Updating dist values', JSON.stringify(config));

        const distBuildFolder = utils.getDistFolder(options);
        const gulpBuildCommand = options.basic ? 'build-basic' : 'build-planner';

        return gulp.series(
            utils.callTask(gulpBuildCommand, options.rebuild),
            utils.deleteFolder(customDistFolder),
            utils.copyFolder(distBuildFolder, customDistFolder),
            gulp.parallel(
                (done) => {
                    gulp
                        .src([`${customDistFolder}/*.js`, `${customDistFolder}/index.html`])
                        .pipe(replace(DEFAULT_HEADERS.title, labels.title || DEFAULT_HEADERS.title))
                        .pipe(replace(DEFAULT_HEADERS.header, labels.header || DEFAULT_HEADERS.header))
                        .pipe(replace(DEFAULT_HEADERS.company, labels.company || DEFAULT_HEADERS.company))
                        .pipe(gulp.dest(customDistFolder, { overwrite: true }))
                        .on('end', done);
                },
                utils.copyFile('logo_company', logoFile, `${customDistFolder}/assets/images`),
                utils.copyFile('package', './__scripts__/package.json', customDistFolder),
            ),
            (done) => {
                gulp
                    .src(`${customDistFolder}/package.json`)
                    .pipe(replace('@simtech/vrp-frontend-dist', config.packageName))
                    .pipe(gulp.dest(customDistFolder, { overwrite: true }))
                    .on('end', done);
            },
        )(done);
    };
}

function npmPublish(distFolder) {
    return (done) => {
        utils.printInfo('Publishing', distFolder);

        return gulp.series(
            utils.runInShell(`cd ${distFolder} && npm publish`),
        )(done);
    };
}

function zipFolder(folder) {
    const options = { zip: args.zip };

    return (done) => {
        if (!options.zip) {
            done();
        } else {
            return gulp.series(
                utils.zip([`${folder}/**/*`], folder),
            )(done);
        }
    };
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { MyDatePickerModule } from 'mydatepicker';

import { PLANNER_ROUTES } from './planner.routes';
import { PlannerDataService } from './services/planner-data.service';
import { PlannerDialogService } from './services/planner-dialog.service';
import { VrpBasicModule } from '@app/vrp-basic';
import { PlannerSharedModule } from '@app/planner/planner-shared.module';
import {
    PlannerOrderDetailDialogComponent,
    PlannerCustomSearchDialogComponent,
    PlannerJobForwardDialogComponent,
    PlannerDateSelectionDialogComponent,
    PlannerItemDetailDialogComponent,
    PlannerVehicleDetailDialogComponent,
    PlannerVehicleTypeDetailDialogComponent,
    PlannerJobListDialogComponent,
    PlannerOrderSplitDialogComponent,
} from '@app/planner/dialogs';

@NgModule({
    imports: [
        PlannerSharedModule,
        RouterModule.forChild(PLANNER_ROUTES),
        MyDateRangePickerModule,
        MyDatePickerModule,
        VrpBasicModule,
    ],
    declarations: [
        PlannerCustomSearchDialogComponent,
        PlannerJobForwardDialogComponent,
        PlannerDateSelectionDialogComponent,
        PlannerItemDetailDialogComponent,
        PlannerVehicleDetailDialogComponent,
        PlannerVehicleTypeDetailDialogComponent,
        PlannerJobListDialogComponent,
        PlannerOrderSplitDialogComponent,
    ],
    providers: [
        PlannerDialogService,
        PlannerDataService,
    ],
    entryComponents: [
        PlannerOrderDetailDialogComponent,
        PlannerCustomSearchDialogComponent,
        PlannerJobForwardDialogComponent,
        PlannerDateSelectionDialogComponent,
        PlannerItemDetailDialogComponent,
        PlannerVehicleDetailDialogComponent,
        PlannerVehicleTypeDetailDialogComponent,
        PlannerJobListDialogComponent,
        PlannerOrderSplitDialogComponent,
    ],
})
export class PlannerModule { }

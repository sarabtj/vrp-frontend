import { Component, HostListener, OnDestroy, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VrpTableComponent } from '@components/vrp-table';
import { TdLoadingService } from '@covalent/core/loading';
import { flatten as _flatten, sortBy as _sortBy, get as _get } from 'lodash-es';
import { finalize } from 'rxjs/operators';
import { Subscription, concat } from 'rxjs';

import { VrpExcelService, VrpFileService, VrpUserRestService, VrpUtils } from '@app/vrp-common';
import { VrpAuthenticationService } from '@app/vrp-common/services/authentication.service';
import { SQL_TABLE_CONFIG } from '@app/planner/planner.config';
import { PlannerJobQuery } from '@app/planner/classes/planner-job-query';
import { PlannerDataService } from '@app/planner/services/planner-data.service';
import { PlannerDialogService } from '@app/planner/services/planner-dialog.service';
import { PlannerRestService, SqlTableNames, DuplicatesTableNames } from '@app/planner/services/planner-rest.service';
import { VrpToastService } from '@app/vrp-common/services/toast.service';
import { STATUS_LABELS } from '@app/planner/planner.config';
import * as  addDays from 'date-fns/add_days';
import * as  format from 'date-fns/format';
import * as  differenceInCalendarDays from 'date-fns/difference_in_calendar_days';

@Component({
    selector: 'vrp-planner-data-management',
    templateUrl: './planner-data-management.component.html',
    styleUrls: ['./planner-data-management.component.scss'],
})
export class PlannerDataManagementComponent implements OnInit, OnDestroy {
    private _unassignedDriverUsers: any[];
    private _allDriverUsers: any[];

    private _firstLoad: boolean = true;
    private filterConditions: any = undefined;
    private _orderTable: DuplicatesTableNames = 'order';
    private _subscriptions: Subscription[] = [];
    private _splitOptions = ['Auto Split', 'Manual Split'];
    private _splitAutoOptions = [];
    private _vehicleTypes = [];

    @ViewChild('vrpTable') private _vrpTable: VrpTableComponent;
    @ViewChild('timeFilterMenu') private _timeFilterMenu;
    showForward: boolean = false;
    @ViewChild('sideNavRight') sideNavRight;
    jobQuery: PlannerJobQuery;

    @Input() selectedSplitType: string;
    @Input() splitCount: number;
    @Input() selectedSplitAutoOptions: string[];

    toolbarDropdownMenu: any[] = [
        { label: 'Download Excel Template', icon: 'description', click: () => { this.downloadExcelTemplate(); } },
        // { label: 'Import from Excel', icon: 'file_upload', click: () =>{ this.importFromExcel() },
        // { label: 'Export to Excel', icon: 'file_download', click: () => { this.exportToExcel(); } },
    ];

    innerHeight: number = 800;

    tableId: string;
    tableData: any = {};
    tableHeight: number = 500;
    tableName: SqlTableNames = 'Vehicle';
    tableList: any[] = [
        { name: 'DeliveryDetail', label: 'Order' },
        { name: 'Vehicle' },
        { name: 'VehicleType', label: 'Vehicle Type' },
        { name: 'Item' },
    ];

    tableItemActions: any[] = [
        { tooltip: 'Open', icon: 'launch', click: (item) => this.updateItem(item) },
    ];

    tableActions: any[] = [
        { tooltip: 'Filter Exceeds Capacity', icon: 'filter', click: () => this._filterExceededCapacityJobs() },
        { tooltip: 'Add New Record', icon: 'add', click: () => this.updateItem() },
        { tooltip: 'Import from Excel', icon: 'file_upload', click: () => this.importFromExcel() },
        { tooltip: 'Export as Excel', icon: 'file_download', click: () => this.exportToExcel() },
    ];

    tableSelectActions: any[] = [
        { tooltip: 'Delete', icon: 'delete', click: (items) => this.deleteItems(items) },
    ];

    readonly columns: any = SQL_TABLE_CONFIG;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _plannerRest: PlannerRestService,
        private _userRest: VrpUserRestService,
        private _authentication: VrpAuthenticationService,
        private _loading: TdLoadingService,
        private _dialog: PlannerDialogService,
        private _excel: VrpExcelService,
        private _file: VrpFileService,
        private _plannerData: PlannerDataService,
        private _toast: VrpToastService,
    ) {
        this._plannerData.addListeners(['DeliveryMaster', 'DeliveryDetail', 'VehicleType', 'Vehicle', 'Item']);
        const today: Date = new Date();
        this.jobQuery = new PlannerJobQuery({ startDate: new Date(today), endDate: new Date(today), finishedJob: false });
        const isPowerPlanner: boolean = this._authentication.isPowerPlanner();
        if (!isPowerPlanner) {
            this.tableList = this.tableList.filter((e) => e.label === 'Order');
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event = undefined) {
        this.innerHeight = window.innerHeight - 65;
        this.tableHeight = window.innerHeight - 105;
    }

    ngOnInit() {
        this.onResize();
        this._subscriptions = [];

        this._route.queryParams.subscribe((p) => {
            if (p.tableName && p.tableName !== this.tableName) {
                this.tableName = p.tableName;
                if (p.tableName !== 'Postal') {
                    if (p.tableName === 'DeliveryDetail') {
                        if (this.tableActions.length < 5) {
                            this.tableActions.push({ tooltip: 'Filter Jobs', img: 'assets/images/filter.png', click: () => this.openTimeFilter() });
                            this.tableActions.push({ tooltip: 'Move to another day', icon: 'forward', click: () => this.moveJobsForward() });
                        }
                        this._plannerData.setDeliveryDetailCachedObject(this.jobQuery);
                    } else {
                        if (this.tableActions.length === 5) {
                            this.tableActions.pop();
                            this.tableActions.pop();
                        }
                        this._plannerData.get(this.tableName).getData$();
                    }
                }
            } else if (this._firstLoad) {
                this._plannerData.get(this.tableName).getData$();
            }
            this._firstLoad = false;
            this._getVehicleTypes();
        });

        // this._subscriptions.push(
        // 	this._plannerData.DeliveryMaster.update$.subscribe((msg) => {
        // 		if (this.tableName === 'DeliveryDetail') {
        // 			console.log(`DeliveryMaster:update`, msg);
        // 			this._vrpTable.filter();
        // 			this._vrpTable.refresh();
        // 		}
        // 	}),
        // );

        ['DeliveryDetail', 'VehicleType', 'Vehicle', 'Item'].forEach((t: SqlTableNames) => {
            const cachedObject = this._plannerData.get(t);
            this._subscriptions.push(
                cachedObject.data$.subscribe((res) => { // subcribe to listen to data
                    if (res && res['error']) {
                        this.tableData[this.tableName] = [];
                        this._dialog.errorResponse(res);
                        return;
                    }
                    if (this.tableName === t) {
                        console.log(`PlannerDataManagementComponent -> ngOnInit -> ${t}:get`, res);
                        this.tableData[this.tableName] = res;
                    }
                    if (this.tableName === 'DeliveryDetail') {
                        this._checkVehicleOrderCapacity(this.tableData[this.tableName]);
                    }
                }),
                cachedObject.update$.subscribe((msg) => {
                    if (this.tableName === t) {
                        console.log(`PlannerDataManagementComponent -> ngOnInit -> ${t}:update`, t, msg);
                        this._vrpTable.filter();
                        this._vrpTable.refresh();
                    }
                }),
            );
        });
    }

    ngOnDestroy() {
        this._subscriptions.forEach((s) => s.unsubscribe());
        this._plannerData.removeAllListeners();
    }

    refresh() {
        this._dialog.confirm('REFRESH_CHANGES_CONFIRM_MSG').subscribe((yes) => {
            if (yes) {
                if ('Postal' === this.tableName as string) {
                    this._plannerData.postalTabRefreshed$.next(true);
                } else {
                    this._plannerData[this.tableName].refresh();
                    this._vrpTable.cancelSelection();
                    this._vrpTable.clearSearchField();
                    this._vrpTable.refresh();
                }
            }
        });
    }

    async updateItem(item = undefined) {
        const isCreateNew: boolean = (item === undefined);
        const isPowerPlanner: boolean = this._authentication.isPowerPlanner();
        switch (this.tableName) {
            case 'DeliveryDetail':
                this._dialog.openOrderDetailById(isCreateNew ? undefined : item.DeliveryMasterId, false);
                break;
            case 'Vehicle':
                await this._updateUnassignedDriverUsernames(item);
                console.log('PlannerDataManagementComponent -> asyncupdateItem -> this._unassignedDriverUsers', this._unassignedDriverUsers);
                this._dialog.openVehicleDetail(item, this._unassignedDriverUsers, isPowerPlanner);
                break;
            case 'VehicleType':
                const vehicleTypeNames = this.tableData.VehicleType.map((i) => i.Name);
                this._dialog.openVehicleTypeDetail(item, vehicleTypeNames);
                break;
            case 'Item':
                const itemIds = this.tableData.Item.map((i) => i.Id);
                this._dialog.openItemDetail(item, itemIds);
                break;
        }
    }

    openTimeFilter() {
        this.sideNavRight.open();
    }

    async moveJobsForward() {
        const targetOrders = this.tableData[this.tableName].filter((d) => d.Status === STATUS_LABELS.indexOf('UNSUCCESSFUL') || (!d.VehicleId && d.Status === STATUS_LABELS.indexOf('PENDING')));
        await this._filterDuplicateRetry(targetOrders);
        this._vrpTable.selectRows(targetOrders);
        this._vrpTable.filter(targetOrders);
        this.tableActions[4] = { tooltip: 'Cancel Job Forwarding', img: 'assets/images/cancel_forward.png', click: () => this.cancelForward() };
        this.showForward = true;
    }

    exportForwardJobs() {
        this.tableActions[4] = { tooltip: 'Move to another day', icon: 'forward', click: () => this.moveJobsForward() };
        const fileName: string = `forward_orders.xlsx`;
        const deliveryItems = this._addOderIdToItems(this._vrpTable.selectedRows);
        const data: any = { Order: this._vrpTable.selectedRows, DeliveryItem: deliveryItems };
        const mappingConfig: any = { Order: this.columns['Order-out'], DeliveryItem: this.columns['DeliveryItem'] };
        this._excel.jsonToExcelFile(data, mappingConfig, fileName);
    }

    cancelForward() {
        this._vrpTable.cancelSelection();
        this._vrpTable.refresh();
        this._vrpTable.filter(this.tableData[this.tableName]);
        this.tableActions[4] = { tooltip: 'Move to another day', icon: 'forward', click: () => this.moveJobsForward() };
        this.showForward = false;
    }

    showForwardDialog() {
        this._dialog.openJobForwardSelection(addDays(new Date(), 1)).subscribe((answer) => {
            this.showForward = false;
            this.tableActions[4] = { tooltip: 'Move to another day', icon: 'forward', click: () => this.moveJobsForward() };
            const allQueries = [];
            const jobIds = this._vrpTable.selectedRows.map((j) => j.DeliveryMasterId);
            const uniqJobIds = Array.from(new Set(jobIds));
            if (answer) {
                uniqJobIds.forEach((id) => {
                    const jobs = this._vrpTable.selectedRows.filter((f) => f.DeliveryMasterId === id);
                    const d = jobs[0];
                    if (!d.VehicleId && d.Status === STATUS_LABELS.indexOf('PENDING')) {
                        if (!d.DeliveryMaster) {
                            d.DeliveryMaster = {};
                            d.DeliveryMaster.Id = d.DeliveryMasterId;
                            d.DeliveryMaster.Priority = answer.priority;
                        } else {
                            d.DeliveryMaster.Priority = answer.priority;
                        }
                        allQueries.push(this._plannerRest.update(d.DeliveryMaster, 'DeliveryMaster'));
                        jobs.forEach((j) => {
                            j.StartTimeWindow = this._changeDay(answer.forwardDate, j.StartTimeWindow);
                            j.EndTimeWindow = this._changeDay(answer.forwardDate, j.EndTimeWindow);

                            allQueries.push(this._plannerRest.update(j, 'DeliveryDetail'));
                        });
                    } else if (d.Status === STATUS_LABELS.indexOf('UNSUCCESSFUL')) {
                        const newOrder = this._resetRetryOrder(jobs, answer);
                        newOrder.Priority = answer.priority;
                        allQueries.push(this._plannerRest.create(newOrder, 'DeliveryMaster'));
                    }
                });
                let successCount = 0;
                concat(...allQueries).subscribe((success) => {
                    successCount++;
                    if (allQueries.length === successCount) {
                        this._toast.shortAlert(`Selected Orders have been moved successfully`);
                    }
                    this._vrpTable.selectedRows = [];
                    this._vrpTable.refresh();
                }, (err) => this._toast.shortAlert(`Failed to save order`, err), () => console.log('complete', successCount));
            } else {
                this._vrpTable.cancelSelection();
            }
        });

    }

    exportToExcel() {
        const fileName: string = `data_${this.tableName}.xlsx`;

        if (this.tableName === 'DeliveryDetail') {
            const deliveryItems = this._addOderIdToItems(this._vrpTable.filteredData);
            const data: any = { Order: this._vrpTable.filteredData, DeliveryItem: deliveryItems };
            const mappingConfig: any = { Order: this.columns['Order-out'], DeliveryItem: this.columns['DeliveryItem'] };
            this._excel.jsonToExcelFile(data, mappingConfig, fileName);
        } else {
            this._excel.jsonToSingleSheetExcelFile(this._vrpTable.filteredData, this.columns[this.tableName], this.tableName, fileName);
        }
    }

    importFromExcel() {
        if (this._authentication.isRestrictedPlanner()) {
            this._dialog.alert(`Restricted planners are not allowed to import data`);
        } else {
            this._dialog.openFileDialog({ accept: '.xlsx,.xls', type: 'binary' }, (result) => {
                const excelData = this._excel.workbookToJson(result, Object.assign({ DeliveryItem: [{ name: 'OrderId' }, { name: 'ItemId' }, { name: 'Quantity' }] }, this.columns));
                this._plannerData.importedExcel = excelData;
                this._router.navigate(['planner', 'data-import'], { queryParams: { canGoBack: true, backUrl: this._router.url } });
            });
        }
    }

    deleteItems(items) {
        let tName: SqlTableNames = this.tableName;

        this._dialog.confirm(`Are you sure to delete ${items.length} selected item(s)?`).subscribe((yes) => {
            if (yes) {
                let ids = items.map((i) => i.Id);
                if (tName === 'DeliveryDetail') {
                    tName = 'DeliveryMaster';
                    ids = items.map((i) => i.DeliveryMasterId);
                    console.log('deleteItems -> ids', ids, tName);
                }

                this._loading.register('planner-data-management.load');
                this._plannerRest.deleteMany(ids, tName).pipe(
                    finalize(() => this._loading.resolve('planner-data-management.load')),
                ).subscribe((res) => console.log('deleteItems', res),
                    (err) => this._toast.shortAlert('Delete Error', err));
            }
        });
    }

    onDateRangeChange(range) {
        this._vrpTable.selectedRows = [];
        this._plannerData.setDeliveryDetailCachedObject(this.jobQuery);
    }

    downloadExcelTemplate() {
        this._plannerRest.getExcelTemplate().subscribe((res) => this._file.saveBlobAs(res, `data_template.xlsx`), (err) => this._dialog.errorResponse(err));
    }

    applyTableFilter(filterConditions: any) {
        this.sideNavRight.close();
        if (filterConditions && Object.keys(filterConditions).length > 0) {
            this.filterConditions = filterConditions;
            const filteredData = VrpUtils.applyFilterToColumns(this.tableData[this.tableName], filterConditions, STATUS_LABELS);
            this._vrpTable.filter(filteredData);
        }
    }

    tabSelect(item) {
        if ('DeliveryDetail' !== item) {
            this.sideNavRight.close();
            this.showForward = false;
            if (this.tableActions.length === 5) {
                this.tableActions[4] = { tooltip: 'Move to another day', icon: 'forward', click: () => this.moveJobsForward() };
            }
            this._vrpTable.cancelSelection();
        }
    }

    private async  _updateUnassignedDriverUsernames(vehicleTobeIncluded: any = undefined) {
        const assignedDriverUsers = this.tableData.Vehicle.filter((v) => v.DriverUsername).map((v) => v.DriverUsername.toUpperCase());

        if (!this._allDriverUsers) {
            try {
                this._allDriverUsers = await this._userRest.getAllDriverUsers().toPromise();
                this._allDriverUsers.forEach((u) => u.username = u.username.toUpperCase());
            } catch (err) {
                this._dialog.errorResponse(err);
            }
        }

        // console.log('privateasync_updateUnassignedDriverUsernames -> this._allDriverUsers', this._allDriverUsers);
        this._unassignedDriverUsers = this._allDriverUsers.filter((d) => {
            const username: string = d.username;
            if (vehicleTobeIncluded && vehicleTobeIncluded.DriverUsername && (username === vehicleTobeIncluded.DriverUsername.toUpperCase())) {
                return true;
            } else {
                return !assignedDriverUsers.includes(username);
            }
        });
    }

    private _changeDay(day, oldDate) {
        const noOfdays = differenceInCalendarDays(new Date(day), new Date(oldDate));
        return format(addDays(oldDate, noOfdays));
    }

    private _addOderIdToItems(orders) {
        const deliveryItems: any = [];
        const Ids: any = [];
        orders.forEach((dd) => {
            if (!Ids.includes(dd.DeliveryMasterId)) {
                Ids.push(dd.DeliveryMasterId);
                dd.DeliveryItems.forEach((di) => {
                    di.DeliveryMasterId = dd.DeliveryMasterId;
                    deliveryItems.push(di);
                });
            }
        });
        return deliveryItems;
    }

    private _resetRetryOrder(jobs, answer) {
        const order = jobs[0];
        const retryOrder: any = { DeliveryDetails: [] };
        const dd: any = { DeliveryItems: [] };
        if (order.DeliveryMasterId.indexOf('retry') !== -1) {
            const subId = order.DeliveryMasterId.split('_retry');
            retryOrder.Id = subId[0] + '_retry' + (Number(subId[1]) + 1);
        } else {
            retryOrder.Id = order.DeliveryMasterId + '_retry1';
        }
        retryOrder.CustomerName = order.DeliveryMaster.CustomerName;
        retryOrder.CustomerPhone = order.DeliveryMaster.CustomerPhone;
        retryOrder.CustomerEmail = order.DeliveryMaster.CustomerEmail;
        jobs.forEach((j) => {

            ['ContactName', 'ContactPhone', 'ContactEmail',
                'JobType', 'JobSequence', 'ServiceTime', 'Address',
                'Postal', 'Lat', 'Lng'].forEach((k) => dd[k] = j[k]);
            dd.StartTimeWindow = this._changeDay(answer.forwardDate, j.StartTimeWindow);
            dd.EndTimeWindow = this._changeDay(answer.forwardDate, j.EndTimeWindow);

            j.DeliveryItems.forEach((i) =>
                dd.DeliveryItems.push({ ItemId: i.ItemId, ItemQty: i.ItemQty }),
            );
            retryOrder.DeliveryDetails.push(dd);
        });

        return retryOrder;
    }

    private _filterDuplicateRetry(jobs) {
        const retryNewJobIds = jobs.map((j) => {
            if (j.DeliveryMasterId.indexOf('retry') !== -1) {
                const subId = j.DeliveryMasterId.split('_retry');
                return subId[0] + '_retry' + (Number(subId[1]) + 1);
            }
            if (j.Status === STATUS_LABELS.indexOf('UNSUCCESSFUL')) {
                return j.DeliveryMasterId + '_retry1';
            }
        });

        const ids: string[] = retryNewJobIds.filter((r) => r !== undefined);
        if (ids.length > 0) {
            return this._plannerRest.checkForDuplicates(ids, this._orderTable).toPromise().then((duplicates) => {
                retryNewJobIds.forEach((id, index) => {
                    if (duplicates.includes(id)) {
                        jobs.splice(index, 1);
                    }
                });
            });
        }
    }

    private async _checkVehicleOrderCapacity(orders: any[]) {
        await this._getVehicleTypes();
        const vehicle = _sortBy(this._vehicleTypes, (v) => -v.Capacity)[0];
        orders.forEach((v) => {
            const items = v.DeliveryItems.reduce((obj, item) => {
                obj[item.Id] = obj[item.Id] || 0;
                obj[item.Id] += item.ItemQty;
                return obj;
            }, {});

            // check if any of items has exceeded vehicles
            for (const i in items) {
                if (items[i] > vehicle.Capacity) {
                    v._exceededCapacity = true;
                }
            }
        });
    }

    private async _getVehicleTypes() {
        this._vehicleTypes = await this._plannerRest.getVehicleTypes(true).toPromise();
        this._splitAutoOptions = [
           // {label: 'Max Filling', value: 'MAX_FILLING'},
            ...this._vehicleTypes.map((vehicleType) => ({
                label: vehicleType.Name,
                value: vehicleType.Id,
            })),
        ];
    }

    private async _filterExceededCapacityJobs() {
        await this._checkVehicleOrderCapacity(this._vrpTable.data);
        this._vrpTable.filteredData = this._vrpTable.data.filter((order) => order._exceededCapacity);
    }

    private async _splitSelectedOrders() {
        this._dialog.openOrderSplit(
            this._vrpTable.selectedRows.filter((order) => order._exceededCapacity),
            {
                splitType: this.selectedSplitType === 'Manual Split' ? 'manual' : 'auto',
                splitAutoOptions: this.selectedSplitAutoOptions,
                splitCount: this.splitCount,
            },
        );
    }

    private _checkExceededOrderCapacity(): boolean {
        const vehicle = _sortBy(this._vehicleTypes, (v) => -v.Capacity)[0];

        if (this._vrpTable && this._vrpTable.data) {
            return (
                this._vrpTable.filteredData.filter((v) => {
                    const items = v.DeliveryItems.reduce((obj, item) => {
                        obj[item.Id] = obj[item.Id] || 0;
                        obj[item.Id] += item.ItemQty;
                        return obj;
                    }, {});

                    // check if any of items has exceeded vehicles
                    for (const i in items) {
                        if (items[i] < vehicle.Capacity) {
                            return false;
                        }
                    }

                    return true;
                })
            ).length === this._vrpTable.filteredData.length &&
            this._vrpTable.selectedRows.length > 0;
        }

        return false;
    }
}

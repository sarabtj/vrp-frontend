import { NgModule } from '@angular/core';

import { PlannerSharedModule } from '@app/planner/planner-shared.module';
import { PlannerDriverPerformanceComponent } from './planner-driver-performance.component';
import { PlannerDriverPerformanceRoutingModule } from './planner-driver-performance-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
@NgModule({
    imports: [
        PlannerSharedModule,
        PlannerDriverPerformanceRoutingModule,
        NgxChartsModule,
    ],
    declarations: [
        PlannerDriverPerformanceComponent,
    ],
})
export class PlannerDriverPerformanceModule { }

import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as  addDays from 'date-fns/add_days';
import * as isSameDay from 'date-fns/is_same_day';

import eachDay from 'date-fns/each_day';
import format from 'date-fns/format';
import { PlannerJobQuery } from '@app/planner/classes/planner-job-query';
import { PlannerDataService } from '@app/planner/services/planner-data.service';
import { PlannerDialogService } from '@app/planner/services/planner-dialog.service';
import { STATUS_LABELS } from '@app/planner/planner.config';

@Component({
    selector: 'vrp-planner-driver-performance',
    templateUrl: './planner-driver-performance.component.html',
    styleUrls: ['./planner-driver-performance.component.scss'],
})
export class PlannerDriverPerformanceComponent implements OnInit, OnDestroy {

    private _subscriptions: Subscription[] = [];

    tableHeight: number = 5;

    dataModel: any = {
        driverSummary: [],
        jobTimeView: [],
        ratingTimeView: [],
    };

    yPercentOfJobs = 'Jobs Completion Percent';
    yNoOfJobs = 'Jobs Loaded';
    timeScale = 'Time period';
    yComparelabel = 'Drivers';
    yRatingAvg = 'Average Rating';

    ratingTimeView: any[] = [];
    jobTimeView: any[] = [];
    ratings: any[] = [];
    allDrivers: any[];
    driverSummary: any[] = [];
    legendItems: any[] = [];
    jobLegendItems = [];
    jobSummary: any[] = [];
    data: any[] = [];
    ratingLegends: any[];
    jobData: any[];

    view: any[] = [450, 180];
    view2: any[] = [480, 180];
    driverView: any[] = [1450, 400];
    compareView: any[] = [650, 400];
    ratingView: any[] = [400, 180];

    colorScheme = {
        domain: ['#62F442', '#D77823', '#AD2121'],
    };
    ratingColorScheme = { domain: ['#ffcc00'] };
    items = [{ name: 'Drivers', selected: [] }];

    jobQuery: PlannerJobQuery;

    constructor(
        private _dialog: PlannerDialogService,
        private _plannerData: PlannerDataService,

    ) {
        this._plannerData.addListeners(['DeliveryDetail']);
        this.jobQuery = new PlannerJobQuery({ startDate: addDays(new Date(), -7), endDate: new Date(), finishedJob: true });
    }

    ngOnInit() {
        this._plannerData.setDeliveryDetailCachedObject(this.jobQuery);
        const t = this._plannerData.DeliveryDetail;
        this._subscriptions = [
            t.data$.subscribe((res) => {
                console.debug('DeliveryDetail:get', res);
                if (res && res['error']) {
                    this.data = [];
                    this._dialog.errorResponse(res);
                    return;
                }
                this.data = res;
                this._setDataFields();
            }),

            t.update$.subscribe((res) => {
                console.debug('DeliveryDetail:update', res);
            }),
        ];
    }

    propagateChange = (_: any) => { };

    writeValue(val: any): void {
        // this.restrictionValue = val;
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
        // throw new Error("Method not implemented.");
    }

    onSelectionChange(group, values) {
        this.items[0].selected = values;
        this._reloadPlots();
    }

    onSelect(event) {
        this.openAllJobs([]);
    }

    openJobs(jobs: any[], title: string = undefined) {
        if (jobs && jobs.length > 0) {
            if (jobs.length === 1) {
                this._dialog.openOrderDetailById(jobs[0].DeliveryMasterId, false);
            } else {
                this._dialog.openJobList(jobs, title);
            }
        }
    }

    openAllJobs(v: any) {
        this.openJobs(v, `All ontime Jobs`);
    }

    ngOnDestroy() {
        this._subscriptions.forEach((s) => s.unsubscribe());
        this._plannerData.removeAllListeners();
    }

    refresh() {
        this._dialog.confirm('REFRESH_CHANGES_CONFIRM_MSG').subscribe((yes) => {
            if (yes) {
                this._plannerData.DeliveryDetail.refresh();
            }
        });

    }

    onDateRangeChange(range) {
        this._plannerData.setDeliveryDetailCachedObject(this.jobQuery);
    }

    private _setDataFields() {
        this.allDrivers = [...new Set(this.data.map((s) => s.DeliveryMaster.LastAttemptedByDriver))];

        this.items[0].selected = [this.allDrivers[0]];
        this.allDrivers.forEach((driver) => {
            const onTime = this._getStatusCount('ONTIME', driver);
            const late = this._getStatusCount('LATE', driver);
            const failure = this._getStatusCount('UNSUCCESSFUL', driver);
            const total = onTime + late + failure;
            this.dataModel.driverSummary.push({
                name: driver, series: [{
                    name: 'ONTIME', value: this._getPercent(onTime, total),
                }, { name: 'LATE', value: this._getPercent(late, total) },
                { name: 'UNSUCCESSFUL', value: this._getPercent(failure, total) }],
            });

            this.dataModel.jobTimeView.push({ name: driver, series: this._getLoadingData(driver) });
            this.dataModel.ratingTimeView.push({ name: driver, series: this._loadingRatingSummary(driver) });
        });
        this._reloadPlots();
    }

    private _reloadPlots() {
        this.driverSummary = this.dataModel.driverSummary.filter((e) => this.items[0].selected.includes(e.name));
        this.jobTimeView = this.dataModel.jobTimeView.filter((e) => this.items[0].selected.includes(e.name));
        this.ratingTimeView = this.dataModel.ratingTimeView.filter((e) => this.items[0].selected.includes(e.name));
        this._refreshJobSummary();
        this._refreshRatingSummary();
        this._refreshJobDistribution();
    }

    private _refreshJobDistribution() {
        this.jobData = [{ name: 'Jobs', series: this._getLoadingData() }];
        const value = this.jobData[0].series.map((s) => s.value).reduce((e, v) => e + v);
        this.jobLegendItems = [{ label: 'Jobs', value: value, color: '#62F442' }];
    }

    private _getLoadingData(driver: any = undefined) {
        const days = eachDay(this.jobQuery.startDate, this.jobQuery.endDate).map((e) => format(e, 'MM/DD/YYYY'));
        const series = [];
        if (driver) {
            days.forEach((d) => series.push({ name: d, value: this.data.filter((e) => driver === e.DeliveryMaster.LastAttemptedByDriver && isSameDay(e.StartTimeWindow, d)).length }));
        } else {
            days.forEach((d) => series.push({ name: d, value: this.data.filter((e) => this.items[0].selected.includes(e.DeliveryMaster.LastAttemptedByDriver) && isSameDay(e.StartTimeWindow, d)).length }));
        }
        return series;
    }

    private _refreshRatingSummary() {
        const fiveStar = this._getRating(5);
        const fourStar = this._getRating(4);
        const threeStar = this._getRating(3);
        const twoStar = this._getRating(2);
        const oneStar = this._getRating(1);
        const starTotal = fiveStar + fourStar + threeStar + twoStar + oneStar;
        const avgStar = (fiveStar * 5 + fourStar * 4 + threeStar * 3 + twoStar * 2 + oneStar) / starTotal;
        this.ratings = [
            { name: '5 Star', value: fiveStar }, { name: '4 Star', value: fourStar }, {
                name: '3 Star', value: threeStar,
            }, { name: '2 Star', value: twoStar }, {
                name: '1 Star', value: oneStar,
            }];
        this.ratingLegends = [{ label: starTotal, value: avgStar.toFixed(2) }];
    }

    private _loadingRatingSummary(driver) {
        const days = eachDay(this.jobQuery.startDate, this.jobQuery.endDate).map((e) => format(e, 'MM/DD/YYYY'));
        const series = [];
        days.forEach((d) => {
            series.push({ name: d, value: this._getAvgRating(d, driver) });
        });
        return series;
    }

    private _getAvgRating(day, driver) {
        const result = { 5: 0, 4: 0, 3: 0, 2: 0, 1: 0 };
        const ratings = this.data.filter((e) => driver === e.DeliveryMaster.LastAttemptedByDriver && isSameDay(e.StartTimeWindow, day) && e.DeliveryPOD && e.DeliveryPOD.customerRating).map((r) => r.DeliveryPOD.customerRating);
        ratings.forEach((v) => result[v] = result[v] + 1);
        const value = (result['5'] * 5 + result['4'] * 4 + result['3'] * 3 + result['2'] * 2 + result['1'] * 1) / (result['5'] + result['4'] + result['3'] + result['2'] + result['1']);
        return Number.isNaN(value) ? 0 : value.toFixed(2);
    }

    private _refreshJobSummary() {
        const onTime = this._getStatusCount('ONTIME');
        const late = this._getStatusCount('LATE');
        const failure = this._getStatusCount('UNSUCCESSFUL');
        const total = onTime + late + failure;

        this.jobSummary = [
            { name: 'ONTIME', value: onTime },
            { name: 'LATE', value: late },
            { name: 'UNSUCCESSFUL', value: failure },
        ];

        this.legendItems = [{ label: 'Ontime', value: '' + onTime, percent: ((onTime / total) * 100).toFixed(2) + '%', color: '#62F442' },
        { label: 'Late', value: '' + late, percent: ((late / total) * 100).toFixed(2) + '%', color: '#D77823', },
        { label: 'Unsuccessful', value: '' + failure, percent: ((failure / total) * 100).toFixed(2) + '%', color: '#AD2121' }];
    }

    private _getRating(value) {
        return this.data.filter((e) => this.items[0].selected.includes(e.DeliveryMaster.LastAttemptedByDriver) && e.DeliveryPOD && e.DeliveryPOD.customerRating === value).length;
    }

    private _getStatusCount(status, driver: string = undefined) {
        if (driver) {
            return this.data.filter((e) => driver === e.DeliveryMaster.LastAttemptedByDriver && STATUS_LABELS[e.Status] === status).length;
        } else {
            return this.data.filter((e) => this.items[0].selected.includes(e.DeliveryMaster.LastAttemptedByDriver) && STATUS_LABELS[e.Status] === status).length;
        }

    }

    private _getPercent(value, total) {
        return ((value / total) * 100).toFixed(2);
    }
}

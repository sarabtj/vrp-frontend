import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlannerDriverPerformanceComponent } from './planner-driver-performance.component';

const routes: Routes = [
    { path: '', component: PlannerDriverPerformanceComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PlannerDriverPerformanceRoutingModule { }

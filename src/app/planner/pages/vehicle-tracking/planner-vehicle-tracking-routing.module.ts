import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlannerVehicleTrackingComponent } from './planner-vehicle-tracking.component';
import { CanDeactivateGuardService } from '@app/planner/services/can-deactivate-guard.service';

const routes: Routes = [
    { path: '', component: PlannerVehicleTrackingComponent, canDeactivate: [CanDeactivateGuardService] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
        CanDeactivateGuardService,
    ],
})
export class PlannerVehicleTrackingRoutingModule { }

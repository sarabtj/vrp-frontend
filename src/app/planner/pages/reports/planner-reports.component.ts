import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import * as isSameDay from 'date-fns/is_same_day';
import eachDay from 'date-fns/each_day';
import format from 'date-fns/format';

import { VrpExcelService, VrpFileService, VrpUtils } from '@app/vrp-common';
import { VrpTableComponent } from '@components/vrp-table';
import { PlannerJobQuery } from '@app/planner/classes/planner-job-query';
import { PlannerDataService } from '@app/planner/services/planner-data.service';
import { TRANSACTION_LOG_TABLE_CONFIG, STATUS_LABELS } from '@app/planner/planner.config';
import { PlannerDialogService } from '@app/planner/services/planner-dialog.service';
import { PlannerRestService } from '@app/planner/services/planner-rest.service';

@Component({
    selector: 'vrp-planner-reports',
    templateUrl: './planner-reports.component.html',
    styleUrls: ['./planner-reports.component.scss'],
})
export class PlannerReportsComponent implements OnInit, OnDestroy {

    private _subscriptions: Subscription[] = [];
    private filterConditions: any = undefined;

    @ViewChild('timeFilterMenu') private _timeFilterMenu;
    @ViewChild('vrpTable') private _vrpTable: VrpTableComponent;
    @ViewChild('sideNavRight') sideNavRight;

    tableHeight: number = 5;
    data: any[];

    jobQuery: PlannerJobQuery;

    summaryText: string = '';
    xTimeScale = 'Time Period';
    yJobDistribution = 'Jobs Loaded';
    yVehicleUsage = 'Vehicles Utilized';
    yCapacityUsage = 'Capacity Utilization percent';

    downloadInProgress: boolean = false;
    disableReports: boolean = false;
    view: any[] = [480, 300];
    smallView: any[] = [440, 300];
    colorScheme = {
        domain: ['#62F442', '#D77823', '#AD2121'],
    };
    VehicleColorScheme = { domain: ['#62F442'], };
    comboBarScheme = { domain: ['#D77823'] };

    legendItems: any[] = [];
    jobSummary: any[] = [];
    jobLegendItems = [];
    jobData: any[];
    vehicleLegendItems: any[] = [];
    vehicles: any[] = [];
    vehicleTypes: any[] = [];
    items: any[] = [];
    yRightData: any[] = [];
    vehicleUsageData: any[] = [];

    tableItemActions: any[] = [
        { tooltip: 'Open', icon: 'launch', click: (item) => this._dialog.openOrderDetailById(item.DeliveryMasterId) },
    ];

    tableActions: any[] = [
        { label: 'Delivery Time Filter', menuTemplateRef: () => this._timeFilterMenu },
        { tooltip: 'Filter Jobs', img: 'assets/images/filter.png', click: () => this.openTimeFilter() },
    ];

    readonly columns: any[] = TRANSACTION_LOG_TABLE_CONFIG;

    constructor(
        private _plannerRest: PlannerRestService,
        private _dialog: PlannerDialogService,
        private _plannerData: PlannerDataService,
        private _file: VrpFileService,
        private _excel: VrpExcelService,
    ) {
        this._plannerData.addListeners(['DeliveryDetail', 'VehicleType', 'Vehicle', 'Item']);
        this.jobQuery = new PlannerJobQuery({ startDate: new Date(), endDate: new Date(), finishedJob: true });
    }

    @HostListener('window:resize', ['$event'])
    onResize(event = undefined) {
        this.tableHeight = window.innerHeight - 105;
    }

    ngOnInit() {
        this.onResize();
        this._plannerData.setDeliveryDetailCachedObject(this.jobQuery);
        const vehicleObject = this._plannerData.Vehicle;
        const t = this._plannerData.DeliveryDetail;
        const vehicleTypeObj = this._plannerData.VehicleType;
        const itemObj = this._plannerData.Item;
        vehicleObject.refresh();
        vehicleTypeObj.refresh();
        itemObj.refresh();
        this._subscriptions = [
            t.data$.subscribe((res) => {
                console.debug('DeliveryDetail:get', res);
                // error also sent in subject
                if (res && res['error']) {
                    this.data = [];
                    this._dialog.errorResponse(res);
                    return;
                }
                this.data = res;
                this._reloadPlots();
                this._updateSummaryText();
            }),
            t.update$.subscribe((res) => {
                this._vrpTable.filter();
                this._vrpTable.refresh();
                this._reloadPlots();
            }),
            vehicleObject.data$.subscribe((res) => { // subscribe to listen to data
                if (res && res['error']) {
                    return;
                }
                this.vehicles = res;

            }),
            vehicleTypeObj.data$.subscribe((res) => { // subscribe to listen to data
                if (res && res['error']) {
                    return;
                }
                this.vehicleTypes = res;
            }),
            itemObj.data$.subscribe((res) => { // subscribe to listen to data
                if (res && res['error']) {
                    return;
                }
                this.items = res;
            }),
        ];
    }

    ngOnDestroy() {
        this._subscriptions.forEach((s) => s.unsubscribe());
        this._plannerData.removeAllListeners();
    }

    refresh() {
        this._dialog.confirm('REFRESH_CHANGES_CONFIRM_MSG').subscribe((yes) => {
            if (yes) {
                this._plannerData.DeliveryDetail.refresh();
                this._vrpTable.cancelSelection();
                this._vrpTable.clearSearchField();
                this._vrpTable.refresh();
            }
        });

    }

    applyDeliveryTimeFilter(timeRange: any) {
        const filteredData = VrpUtils.applyFilterToDateColumn(this.data, 'ActualDeliveryTime', timeRange);
        this._vrpTable.filter(filteredData);
        this.tableActions[0].label = `Filtered (${timeRange.start} to  ${timeRange.end}) `;
    }

    openTimeFilter() {
        this.sideNavRight.open();
    }

    applyTableFilter(filterConditions: any) {
        this.sideNavRight.close();
        if (filterConditions && Object.keys(filterConditions).length > 0) {
            this.filterConditions = filterConditions;
            const filteredData = VrpUtils.applyFilterToColumns(this.data, filterConditions, STATUS_LABELS);
            this._vrpTable.filter(filteredData);
        }
    }

    onDateRangeChange(range) {
        this._vrpTable.selectedRows = [];
        this._plannerData.setDeliveryDetailCachedObject(this.jobQuery);
    }

    exportToExcel() {
        const fileName: string = `${this._getBaseFileName()}.xlsx`;
        this._excel.jsonToSingleSheetExcelFile(this._vrpTable.filteredData, this.columns, 'reports', fileName);
    }

    exportToZip(withPhotos: boolean) {
        this.downloadInProgress = true;
        this._plannerRest.getTransactionLogFile(this.jobQuery.startDate, this.jobQuery.endDate, withPhotos)
            .pipe(finalize(() => this.downloadInProgress = false))
            .subscribe((blob) => {
                this._file.saveBlobAs(blob, `${this._getBaseFileName()}-${withPhotos ? 'withPhoto' : 'noPhoto'}.zip`);
            }, (err) => this._dialog.errorResponse(err));
    }

    private _getBaseFileName(): string {
        return `reports-${this.jobQuery.startDate.YYMMDD()}-${this.jobQuery.endDate.YYMMDD()}`;
    }

    private _updateSummaryText() {
        const nCounts: number[] = STATUS_LABELS.map(() => 0);
        this.data.forEach((row) => nCounts[row.Status]++);
        let s: string = `  ${this.data.length} of job(s) finished, `;
        s += nCounts.map((c, status) => (c > 0) ? `${c} ${STATUS_LABELS[status]}` : undefined).filter((c) => c).join(', ');
        this.summaryText = s;
    }

    private _reloadPlots() {
        this._refreshJobSummary();
        this._refreshJobDistribution();
        this._refreshVehicleUtilization();
    }

    private _refreshJobSummary() {
        const onTime = this._getStatusCount('ONTIME');
        const late = this._getStatusCount('LATE');
        const failure = this._getStatusCount('UNSUCCESSFUL');
        const total = onTime + late + failure;

        this.jobSummary = [
            { name: 'ONTIME', value: onTime },
            { name: 'LATE', value: late },
            { name: 'UNSUCCESSFUL', value: failure },
        ];

        this.legendItems = [{ label: 'Ontime', value: '' + onTime, percent: ((onTime / total) * 100).toFixed(2) + '%', color: '#62F442' },
        { label: 'Late', value: '' + late, percent: ((late / total) * 100).toFixed(2) + '%', color: '#D77823', },
        { label: 'Unsuccessful', value: '' + failure, percent: ((failure / total) * 100).toFixed(2) + '%', color: '#AD2121' }];
    }

    private _refreshVehicleUtilization() {
        this.yRightData = this._getVehicleCapacityData();
        this.vehicleUsageData = [{
            name: 'Vehicles', series: this._getVehicleLoadingData(),
        }];
        const capPercent = (this._getAvg(this.yRightData.map((s) => +s.value * 100))) / 100 + '%';
        const avgVehicles = this._getAvg(this.vehicleUsageData[0].series.map((s) => s.value));
        this.vehicleLegendItems = [
            { value: capPercent, color: '#D77823' },
            { label: avgVehicles, value: this.vehicles.length, percent: this._getPercent(avgVehicles, this.vehicles.length) + '%', color: '#62F442' },
        ];
    }

    private _refreshJobDistribution() {
        this.jobData = [{ name: 'Jobs', series: this._getLoadingData() }];
        const value = this.jobData[0].series.map((s) => s.value).reduce((e, v) => e + v);
        this.jobLegendItems = [{ label: 'Jobs', value: value, color: '#62F442' }];
    }

    private _getLoadingData() {
        const days = eachDay(this.jobQuery.startDate, this.jobQuery.endDate).map((e) => format(e, 'MM/DD/YYYY'));
        const series = [];

        days.forEach((d) => series.push({ name: d, value: this.data.filter((e) => isSameDay(e.StartTimeWindow, d)).length }));
        return series;
    }

    private _getStatusCount(status) {
        return this.data.filter((e) => STATUS_LABELS[e.Status] === status).length;
    }

    private _getVehicleLoadingData() {
        const days = eachDay(this.jobQuery.startDate, this.jobQuery.endDate).map((e) => format(e, 'MM/DD/YYYY'));
        const series = [];
        days.forEach((d) => {
            const sameDayJobs = this.data.filter((e) => isSameDay(e.StartTimeWindow, d));
            series.push({ name: d, value: [...new Set(sameDayJobs.map((s) => s.VehicleId))].length });
        });

        return series;
    }

    private _getPercent(value, total) {
        return ((value / total) * 100).toFixed(2);
    }

    private _getItemWeight(itemId) {
        return this.items.find((i) => i.Id === itemId).Weight;
    }

    private _getTotalCapacity(vehicleIds) {
        let capacity = 0;
        vehicleIds.forEach((vId) => {
            capacity += this.vehicleTypes.find((vt) => vt.Id === (this.vehicles.find((v) => v.Id === vId).VehicleTypeId)).Capacity;
        });
        return capacity;
    }

    private _getAvg(list) {
        return list.reduce((prev, curr) => prev + curr) / list.length;
    }

    private _getVehicleCapacityData() {

        const days = eachDay(this.jobQuery.startDate, this.jobQuery.endDate).map((e) => format(e, 'MM/DD/YYYY'));
        const series = [];

        days.forEach((d) => {
            let itemWeight = 0;
            const sameDayJobs = this.data.filter((e) => isSameDay(e.StartTimeWindow, d));
            if (sameDayJobs.length > 0) {
                const sameDayJobItems = sameDayJobs.map((j) => j.DeliveryItems);
                const availableCapacity = this._getTotalCapacity([...new Set(sameDayJobs.map((s) => s.VehicleId))]);
                sameDayJobItems.forEach((items) => items.forEach((item) => itemWeight += item.ActualItemQty * this._getItemWeight(item.ItemId)));
                series.push({ name: d, value: this._getPercent(itemWeight, availableCapacity) });
            } else {
                series.push({ name: d, value: 0 });
            }
        });

        return series;
    }

}

import { NgModule } from '@angular/core';

import { PlannerSharedModule } from '@app/planner/planner-shared.module';
import { PlannerReportsComponent } from './planner-reports.component';
import { PlannerReportsRoutingModule } from './planner-reports-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ComboChartModule } from '@components/combo-chart';
@NgModule({
    imports: [
        NgxChartsModule,
        ComboChartModule,
        PlannerSharedModule,
        PlannerReportsRoutingModule,
    ],
    declarations: [
        PlannerReportsComponent,
    ],
})
export class PlannerReportsModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlannerReportsComponent } from './planner-reports.component';

const routes: Routes = [
    { path: '', component: PlannerReportsComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PlannerReportsRoutingModule { }

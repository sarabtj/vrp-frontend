import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { PlannerJobQuery } from '@app/planner/classes/planner-job-query';

@Injectable({
    providedIn: 'root',
})

export class PlannerReportsDataService {

    reportsRefreshed$: Subject<boolean> = new Subject(); // fire if monitor tab refresh button clicked
    exportData$: Subject<boolean> = new Subject();  // to download Data
    enableDownload$: Subject<boolean> = new Subject();  // allows to download Data
    editDateRange$: Subject<PlannerJobQuery> = new Subject(); // fires on reports date range click

    constructor() { }
}

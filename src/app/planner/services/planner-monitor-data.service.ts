import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable({
    providedIn: 'root',
})
export class PlannerMonitorDataService {
    dispatchPanelToggled$: Subject<boolean> = new Subject(); // fire if dispatch panel open or close
    monitorRefreshed$: Subject<boolean> = new Subject(); // fire if monitor tab refresh button clicked
    discardDispatchChanges$: Subject<boolean> = new Subject(); // fire to discard dispatch changes from tracking screen
    unsavedRoutes$: boolean = false; // true if there are unsaved routes
    resetDateSetting$: Subject<boolean> = new Subject(); // fire on click of progress/tracking
    constructor() { }
}

import { Component, Input } from '@angular/core';

import { PlannerUtils } from '@app/planner/classes/planner-utils';
import { PlannerRestService } from '@app/planner/services/planner-rest.service';
import { VrpFileService } from '@app/vrp-common';
import { PlannerDialogService } from '@app/planner/services/planner-dialog.service';
import { toLower as _lower } from 'lodash-es';

@Component({
    selector: 'vrp-planner-order-summary',
    templateUrl: './planner-order-summary.component.html',
    styleUrls: ['./planner-order-summary.component.scss'],
})
export class PlannerOrderSummaryComponent {

    @Input() order: any = {};
    disabled: boolean = true;

    constructor(
        private _plannerRest: PlannerRestService,
        private _file: VrpFileService,
        private _dialog: PlannerDialogService,
    ) { }

    downloadServiceChit($event: any, job: any) {
        $event.preventDefault(); // prevents order dialog closing
        this._plannerRest.getServiceChit(job.Id).subscribe((blob) => {
            this._file.saveBlobAs(blob, `servicechit_${job.DeliveryMasterId}_${_lower(job.JobType)}.pdf`);
        }, (err) => this._dialog.errorResponse(err));
    }

    showSeriveChit(job: any) {
        return PlannerUtils.isJobSuccess(job);
    }
}

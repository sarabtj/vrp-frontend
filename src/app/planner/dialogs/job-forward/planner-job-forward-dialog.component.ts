import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';
import * as differenceInCalendarDays from 'date-fns/difference_in_calendar_days';
import { PRIORITY_LABELS } from '@app/planner/planner.config';
import { date } from 'ngx-custom-validators/src/app/date/validator';
import * as  addDays from 'date-fns/add_days';

@Component({
    selector: 'vrp-planner-job-forward-dialog',
    templateUrl: './planner-job-forward-dialog.component.html',
    styleUrls: ['./planner-job-forward-dialog.component.scss'],
})
export class PlannerJobForwardDialogComponent {

    title: string = 'Move Jobs to future Date';

    cancelButton: string = 'CANCEL';
    acceptButton: string = 'SAVE';
    f: FormGroup;
    oldDays = addDays(new Date(), -1);
    myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
        disableUntil: {
            year: this.oldDays.getFullYear(),
            month: this.oldDays.getMonth() + 1,
            day: this.oldDays.getDate(),
        },
        inline: true,
    };

    readonly priorityLabels: string[] = PRIORITY_LABELS.filter((e) => e.length > 0);

    constructor(
        private _dialogRef: MatDialogRef<PlannerJobForwardDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
    ) {

        this.f = new FormGroup({

            forwardDate: new FormControl(undefined, [
                Validators.required,
            ]),
            priority: new FormControl(undefined, [Validators.required, ]),
        });

    }

    save(value) {
        this._dialogRef.close({ forwardDate: value.forwardDate.formatted, priority: value.priority });
    }

}

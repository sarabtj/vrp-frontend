import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {
    remove as _remove,
    find as _find,
    get as _get,
    set as _set,
    flatten as _flatten,
    sortBy as _sortBy,
} from 'lodash-es';

import { VrpTableComponent } from '@components/vrp-table';
import { PlannerRestService } from '@app/planner/services/planner-rest.service';
import { SQL_TABLE_CONFIG } from '@app/planner/planner.config';

@Component({
    selector: 'vrp-planner-order-split-dialog',
    templateUrl: './planner-order-split-dialog.component.html',
    styleUrls: ['./planner-order-split-dialog.component.scss'],
})
export class PlannerOrderSplitDialogComponent {

    title: string = 'Order Split';

    cancelButton: string = 'CANCEL';
    acceptButton: string = 'SAVE';

    @ViewChild('vrpTable') _vrpTable: VrpTableComponent;
    columns = [
        ...SQL_TABLE_CONFIG.DeliveryDetail.filter((v) => !['', '_error', 'VehicleRestriction'].includes(v.label)),
        { name: '_quantity', label: 'Quantity', type: 'number' },
        { name: '_vehicleRestriction', label: 'Vehicle Restriction', type: 'text' },
    ];

    tableData: any[] = [];
    splittedOrders: any[] = [];
    oldOrderIds: any[] = [];

    splitType: 'manual' | 'auto';
    splitCount: number;
    splitAutoOptions: string[];
    splitAutoSelectOptions: any[];
    _vehicleTypes: any[];

    constructor(
        private _dialogRef: MatDialogRef<PlannerOrderSplitDialogComponent>,
        private _plannerRest: PlannerRestService,
        @Inject(MAT_DIALOG_DATA) private data: any,
    ) {
        this.splitType = data.options.splitType;
        this.splitCount = data.options.splitCount;
        this.splitAutoOptions = data.options.splitAutoOptions;

        (async() => {
            this._vehicleTypes = await this._plannerRest.getVehicleTypes(true).toPromise();
            this.tableData = await this._splitSelectedOrders(data.orders || []);
            this.splitAutoSelectOptions = this._vehicleTypes.map((vehicleType) => ({
                label: vehicleType.Name,
                value: vehicleType.Id,
            }));
        })();
    }

    async save() {
        this.splittedOrders.map((order) => {
            _set(order, 'DeliveryDetails.0.DeliveryItems.0.ItemQty', order._quantity);
        });
        await this._plannerRest.create(this.splittedOrders, 'DeliveryMaster').toPromise();
        await this._plannerRest.deleteMany(this.oldOrderIds, 'DeliveryMaster').toPromise();
        this._dialogRef.close();
    }

    onRowsDeleted(items) {

    }

    private async _splitSelectedOrders(orders: any[]): Promise<any[]> {
        this.oldOrderIds = orders.map((v) => v.DeliveryMasterId);
        const isManual = this.splitType === 'manual';
        const splittedOrders = [];
        const vehicles = await this._plannerRest.getVehicles(undefined, true).toPromise();
        const pseries = (list) => {
            const p = Promise.resolve();
            return list.reduce(function(pacc, fn) {
                return pacc = pacc.then(fn);
            }, p);
        };

        const splittedOrdersResult = await Promise.all(orders.map(async(order) => {
            try {
                let lastSplitResult;
                let currentOrderSplit = [];
                const items = order.DeliveryItems.map((v) => ({
                    Id: v.Id,
                    ItemId: v.ItemId,
                }));
                
                await pseries(items.map((item) => async() => {
                    lastSplitResult = await this._applySplitToOrder(order, {
                        splitType: isManual ? 'manual' : 'auto',
                        splitCount: this.splitCount,
                        vehicles: vehicles,
                        itemId: item.Id,
                        itemName: item.ItemId,
                        currentSplitIteration: (
                            lastSplitResult &&
                            _get(lastSplitResult.reverse(), '0.splitResult.currentSplitIteration')
                        ) || 0,
                        // use only selected vehicle types
                        vehicleTypes: isManual ? this._vehicleTypes : this._vehicleTypes.filter(
                            (v) => this.splitAutoOptions.includes(v.Id),
                        ),
                    });

                    currentOrderSplit = currentOrderSplit.concat(lastSplitResult);
                }));
                // const result = await this._applySplitToOrder(order, {
                //     vehicles: vehicles,
                //     // use only selected vehicle types
                //     vehicleTypes: this._vehicleTypes.filter((v) => this.selectedSplitAutoOptions.includes(v.Id)),
                // });

                return _flatten(currentOrderSplit);
            } catch (ex) {
                console.log(ex);
            }
        }));

        _flatten(splittedOrdersResult).forEach((order) => {
            if (order.splitResult.ItemQty > 0) {
                order._isManual = this.splitType === 'manual';
                order._quantity = order.splitResult.ItemQty;
                order._vehicleRestriction = '';

                splittedOrders.push({
                    ...order,
                    CustomerName: order.ContactName,
                    CustomerPhone: order.ContactPhone,
                    Id: order.splitResult.DeliveryId,
                    Priority: order.Priority,
                    DeliveryDetails: [{
                        Address: order.Address,
                        ContactName: order.ContactName,
                        ContactPhone: order.ContactPhone,
                        EndTimeWindow: order.EndTimeWindow,
                        JobSequence: order.JobSequence,
                        JobType: order.JobType,
                        Lat: order.Lat,
                        Lng: order.Lng,
                        Postal: order.Postal,
                        ServiceTime: order.ServiceTime,
                        StartTimeWindow: order.StartTimeWindow,
                        DeliveryItems: [{
                            DeliveryMasterId: order.splitResult.DeliveryId,
                            ItemId: order.splitResult.ItemName,
                            ItemQty: order.splitResult.ItemQty,
                        }],
                    }],
                });
            }
        });

        console.log('SPLIT RESULT');
        console.dir(splittedOrders);

        this.splittedOrders = splittedOrders;
        return splittedOrders;
        // if (splittedOrders.length) {
        //     await this._plannerRest.create(splittedOrders, 'DeliveryMaster').toPromise();
        //     await this._plannerRest.deleteMany(orderIds, 'DeliveryMaster').toPromise();

        //     this._plannerData[this.tableName].refresh();
        //     this._vrpTable.cancelSelection();
        //     this._vrpTable.clearSearchField();
        //     this._vrpTable.refresh();
        // }
    }

    private async _applySplitToOrder(order: any, option: any): Promise<any[]> {
        const isManual = option.splitType === 'manual';
        const vehicles = option.vehicles;
        const vehicleTypes = option.vehicleTypes;
        const orderQuantity = order.DeliveryItems.reduce((sum, v) => {
            return (v.Id === option.itemId ? v.ItemQty : 0) + sum;
        }, 0);
        let result = [];

        if (!option.remainingOrderItemQty) {
            option.remainingOrderItemQty = orderQuantity;
            option.currentSplitIteration = option.currentSplitIteration || 0;
        }

        if (isManual) {
            const vehicle = _sortBy(vehicleTypes, (v) => -v.Capacity)[0];

            if (option.remainingOrderItemQty > 0 && vehicle) {
                if (vehicle.Capacity < option.splitCount) {
                    alert('Split count is less that max cap');
                    return;
                }

                const deliveryId = `${order.DeliveryMasterId}_${++option.currentSplitIteration}`;
                const qty = option.remainingOrderItemQty > vehicle.Capacity ?
                    vehicle.Capacity :
                    option.remainingOrderItemQty % vehicle.Capacity;

                result = result.concat({
                    ...order,
                    splitResult: {
                        DeliveryId: deliveryId,
                        ItemId: option.itemId,
                        ItemName: option.itemName,
                        ItemQty: qty,
                        currentSplitIteration: option.currentSplitIteration,
                    },
                    DeliveryMasterId: deliveryId,
                });

                option.remainingOrderItemQty -= qty;

            } else if (option.currentSplitIteration < this.splitCount) {
                const deliveryId = `${order.DeliveryMasterId}_${++option.currentSplitIteration}`;

                result = result.concat({
                    ...order,
                    splitResult: {
                        DeliveryId: deliveryId,
                        ItemId: option.itemId,
                        ItemName: option.itemName,
                        ItemQty: 0,
                        currentSplitIteration: option.currentSplitIteration,
                    },
                    DeliveryMasterId: deliveryId,
                });
            }

        } else {
            _sortBy(vehicleTypes, (v) => -v.Capacity).forEach((vehicleType) => {
                const currentVehicles = vehicles.filter((v) => v.VehicleTypeId === vehicleType.Id);
                const totalVehicle = currentVehicles.length;

                if (option.remainingOrderItemQty > 0 && totalVehicle) {
                    const N = (option.remainingOrderItemQty / vehicleType.Capacity);

                    for (let i = 0; i < ((N > totalVehicle) ? totalVehicle : N) ; i++) {
                        let qty = 0;
                        const deliveryId = `${order.DeliveryMasterId}_${++option.currentSplitIteration}`;

                        if (!((i + 1) < ((N > totalVehicle) ? totalVehicle : N)) && !(N > totalVehicle)) {
                            qty = option.remainingOrderItemQty % vehicleType.Capacity;
                        } else {
                            qty = vehicleType.Capacity;
                        }

                        result = result.concat({
                            ...order,
                            splitResult: {
                                DeliveryId: deliveryId,
                                ItemId: option.itemId,
                                ItemName: option.itemName,
                                ItemQty: qty,
                                Vehicle: currentVehicles[i].Id,
                                currentSplitIteration: option.currentSplitIteration,
                            },
                            DeliveryMasterId: deliveryId,
                        });

                        option.remainingOrderItemQty -= qty;
                    }
                }
            });
        }

        if (
            option.remainingOrderItemQty > 0 ||
            (option.currentSplitIteration < this.splitCount && isManual)
        ) {
            result = result.concat(await this._applySplitToOrder(order, option));
        }

        return result;
    }

    private _autoOptionsSelectChange(event: any, row: any) {
        const selectedValues = event.value;
        row._vehicleRestriction = selectedValues;
    }

    private _filterVehicleTypeForOrder(vehicleTypes: any[], order: any): any[] {
        return vehicleTypes.filter((vehicleType) => (
            _find(this._vehicleTypes, (v) => v.Id === vehicleType.value).Capacity >= order._quantity
        ));
    }
}

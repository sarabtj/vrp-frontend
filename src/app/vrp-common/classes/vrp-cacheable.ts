
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
declare type GetDataHandler<T> = () => Observable<T>;

export class VrpCacheable<T> {

    cachedData: T;
    dataSubject: Subject<T>;
    data$: Observable<T>;
    error: T;
    updateSubject: Subject<{ data: T, purpose: string }>;
    update$: Observable<{ data: T, purpose: string }>;

    queryObject: any; // to store query info such as date range

    getDataHandler: GetDataHandler<T>;
    shouldDataBeUpdated: (d: any) => boolean;

    constructor() {
        this.dataSubject = new Subject();
        this.data$ = this.dataSubject.asObservable();

        this.updateSubject = new Subject();
        this.update$ = this.updateSubject.asObservable();

        this.shouldDataBeUpdated = () => true;
    }

    static beforeDataQuery: () => any = () => { };
    static afterDataQuery: () => any = () => { };

    getData$() {
        if (!this.getDataHandler) {
            throw new Error('getHandler is not defined');
        }
        if (!this.cachedData) {// if no cachedData, use getHandler to query data
            VrpCacheable.beforeDataQuery();
            this.getDataHandler().pipe(finalize(() => VrpCacheable.afterDataQuery()))
                .subscribe((res: T) => {
                    this.cachedData = res;
                    this.dataSubject.next(res);
                }, (err) => {
                    this.error = err;
                    this.dataSubject.next(this.error); // error has to be sent in subject.next() as subject.error() will terminate the stream
                });
        } else {
            this.dataSubject.next(this.cachedData);
        }
    }

    clearCache(): void {
        this.cachedData = undefined;
    }

    refresh(): void {
        this.clearCache();
        this.getData$();
    }

}

import { NgModule } from '@angular/core';

import { SharedModule } from '@root/shared.module';
import { VrpDialogModule } from '@components/vrp-dialog';
import { VrpTableComponent } from './components/table/vrp-table.component';
import { VrpTimeFilterMenuComponent } from './components/time-filter-menu/vrp-time-filter-menu.component';
import { TableFilterComponent } from './components/table-filter/table-filter.component';
export { VrpTableComponent } from './components/table/vrp-table.component';
import { MatChipsModule } from '@angular/material/chips';

@NgModule({
    imports: [
        SharedModule,
        VrpDialogModule,
        MatChipsModule,
    ],
    declarations: [
        VrpTableComponent,
        VrpTimeFilterMenuComponent,
        TableFilterComponent,
    ],
    exports: [
        VrpTableComponent,
        VrpTimeFilterMenuComponent,
        TableFilterComponent,
    ],
})
export class VrpTableModule { }

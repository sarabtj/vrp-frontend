import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete, MatChipInput } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { FILTER_DEFAULT_VALUES, STATUS_LABELS } from '@app/planner/planner.config';
@Component({
    selector: 'vrp-table-filter',
    templateUrl: './table-filter.component.html',
    styleUrls: ['./table-filter.component.scss'],
})

export class TableFilterComponent implements OnInit {

    @Input() set columns(columns) {
        this._allColumns = columns.map((c) => c.label);
    }

    @Input() set defaults(values) {
        if (values) {
            this._columns = Object.keys(values);
            this.filterContraints = Object.values(values);
        } else {
            this._columns = this._allColumns.splice(1, 3); // selected by default
        }
    }

    @Output() applyTableFilter: EventEmitter<any> = new EventEmitter();

    separatorKeysCodes: number[] = [ENTER, COMMA];
    columnCtrl = new FormControl();
    _columns: string[] = [];
    _allColumns: string[] = [];
    filteredColumns: string[];
    _chipInput: MatChipInput;
    _onChange: any;
    filterContraints: string[] = [];
    filterDefaults: any;

    @ViewChild('columnInput') columnInput: ElementRef<HTMLInputElement>;
    @ViewChild('auto') matAutocomplete: MatAutocomplete;

    constructor() {
    }

    ngOnInit() {
        this.filterDefaults = FILTER_DEFAULT_VALUES;
        this.filteredColumns = this._allColumns;
    }

    add(event: MatChipInputEvent): void {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            const value = event.value;

            if ((value || '').trim()) {
                this._columns.push(value.trim());
            }
            if (input) {
                input.value = '';
            }
            this._filter();
            this.columnCtrl.setValue(undefined);
            this.columnInput.nativeElement.value = '';
        }
    }

    remove(column: string): void {
        const index = this._columns.indexOf(column);

        if (index >= 0) {
            this._columns.splice(index, 1);
            this.filterContraints.splice(index, 1);
        }
        this._filter();
    }

    selected(event: MatAutocompleteSelectedEvent): void {
        this._columns.push(event.option.viewValue);
        this._filter();
        this.columnInput.nativeElement.value = '';
        this.columnCtrl.setValue(undefined);
    }

    applyFilter() {
        const tableFilter = {};
        this._columns.forEach((e, index) => { if (this.filterContraints[index]) { tableFilter[e] = this.filterContraints[index]; } });
        this.applyTableFilter.emit(tableFilter);
    }

    closeFilter() {
        this.applyTableFilter.emit(undefined);
    }

    private _filter() {
        this.filteredColumns = this._allColumns.filter((column) => !this._columns.includes(column));
    }

}

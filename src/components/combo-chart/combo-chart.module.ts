import { NgModule } from '@angular/core';

import { SharedModule } from '@root/shared.module';
import { ComboChartComponent } from './components/combo-chart.component';
import { ComboSeriesVerticalComponent } from './components/combo-series-vertical.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
export { ComboChartComponent, ComboSeriesVerticalComponent };

@NgModule({
    imports: [
        SharedModule,
        NgxChartsModule,
    ],
    declarations: [
        ComboChartComponent,
        ComboSeriesVerticalComponent,
    ],
    exports: [
        ComboChartComponent,
        ComboSeriesVerticalComponent,
    ],
})
export class ComboChartModule { }

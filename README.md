- [Installation](#installation)
    - [Step 1-2: Programs Required](#step-1-2-programs-required)
    - [Step 3-4: Set-up (cmd/terminal)](#step-3-4-set-up-cmdterminal)
    - [Step 5: Development Environment](#step-5-development-environment)
    - [Step 6: Run](#step-6-run)
- [Development](#development)
    - [Projects](#projects)
    - [Structure](#structure)
    - [Libraries](#libraries)
        - [Covalent](#covalent)
    - [Patches](#patches)
    - [Polyfills](#polyfills)
    - [Components](#components)
    - [Services](#services)
- [Deployment](#deployment)
    - [Step 1-2: NPM Publish](#step-1-2-npm-publish)
        - [Potential Issues](#potential-issues)

# Installation

## Step 1-2: Programs Required

Step 1: **Install Nodejs** - Install the latest stable version (at least 8.10.0) of Nodejs via [nodejs.org](https://nodejs.org/en/)

Step 2: **Install Git** - Install the latest version (at least 2.16.0) via [git-scm.com](https://git-scm.com/downloads)

## Step 3-4: Set-up (cmd/terminal)

Step 3: **Clone source code**

`git clone http://<GIT_URL>/root/vrp-frontend.git`

Step 4: **Install the required modules**

1. `cd vrp-frontend`
2. `npm install`

## Step 5: Development Environment

* Recommended IDE
    - Visual Studio Code
* Recommended Extensions
    - TSLint (supports formatting and linting)
    - Debugger for Chrome (supports debugging)
    - TypeScript Hero (supports importing packages/components)

Step 5: Configure Debugger Chrome Extension

1. Run Google Chrome with options `"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --remote-debugging-port=9222`
2. Modify `nano .vscode/launch.json`

Add the following to list of configurations:
```
{
    "name": "Attach to Chrome, with sourcemaps",
    "type": "chrome",
    "request": "attach",
    "port": 9222,
    //"url":"http://localhost:4200",
    "sourceMaps": true,
    "webRoot": "${workspaceRoot}",
    "sourceMapPathOverrides": {
        "webpack:///C:*": "C:/*"
        //"webpack:///*": "/*"
        //"webpack:///./*":"${workspaceRoot}\\*"
    }
}
```

## Step 6: Run

Step 6: **Run project**

1. `npm run start`
2. Open http://localhost:4200 on Google Chrome

# Development

## Projects

There are two types of "apps" in vrp-frontend - `app-basic` and `app-planner`
    * `app-basic` only supports admins and normal users
        * Companies: XDel, Y3
    * `app-plannner` supports admins, normal, driver, planner users
        * Companies: GLS, Impetus

## Structure

All pages are lazy-loaded.

* /app-basic, /app-planner
    * Navigation routing module
* /vrp-common
    * Common modules between `vrp-basic` and `vrp-planner`

## Libraries

### Covalent

* Data Table (i.e. `vrp-table)
* Loading
* Notification
* Search

## Patches

Always check if patch files can be removed after its respective library upgrades

**covalent-core-data-table.js**

Used to fix sizing of hidden columns

## Polyfills

Modified to add interface for `Date`

## Components

| Component           | Purpose                     | Remarks                               |
| ------------------- | --------------------------- | ------------------------------------- |
| `vrp-table`         | All tables                  |                                       |
| `vrp-search-select` | Select list, filter textbox |                                       |
| `vrp-leaflet`       | All maps                    | Marker limitation: Cannot change size |
| `vrp-dialog`        | All dialogs                 | Needs to change to reactive           |

## Services

| Service        | Purpose                            | Remarks                                |
| -------------- | ---------------------------------- | -------------------------------------- |
| `VrpHttpCache` | Store URL as key, response as data |                                        |
| `VrpCacheable` | Reduces HTTP calls to the server   | For `vrp-planner` (i.e. /planner) only |

# Deployment

## Step 1-2: NPM Publish

Step 1: Build distributions

This step compiles the application into `/dist` folder (default).

`npm run build` [https://github.com/angular/angular-cli/wiki/build](https://github.com/angular/angular-cli/wiki/build)

Step 2: Run gulp scripts

1. `npm install glup@4.0.0 -g`
1. `gulp bump`
1. `gulp build`
1. `gulp publish-simtech`

### Potential Issues

Error 1: Out of memory [`FATAL ERROR: CALL_AND_RETRY_LAST Allocation failed - JavaScript heap out of memory`](https://github.com/endel/increase-memory-limit),

1. `npm install -g increase-memory-limit`
1. `increase-memory-limit`
